package com.example.imc_actividad2;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class conversion_Grados extends AppCompatActivity {

    private EditText txtCantidad;
    private RadioGroup rgConversion;
    private RadioButton rdbCel, rdbFa;
    private TextView txtResultado;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion_grados);

        // Inicializar las vistas
        txtCantidad = findViewById(R.id.txtCantidad);
        rgConversion = findViewById(R.id.rgConversion);  // Corregido el ID aquí
        rdbCel = findViewById(R.id.rdbCel);
        rdbFa = findViewById(R.id.rdbFa);
        txtResultado = findViewById(R.id.txtResultado);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);

        // Listener para el botón Calcular
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput = txtCantidad.getText().toString();
                if (tempInput.isEmpty()) {
                    Toast.makeText(conversion_Grados.this, "Por favor, ingrese una cantidad de grados", Toast.LENGTH_SHORT).show();
                    return;
                }

                double cantidad = Double.parseDouble(tempInput);
                double resultado;
                if (rdbCel.isChecked()) {
                    resultado = celsiusToFahrenheit(cantidad);
                    txtResultado.setText(String.format("Resultado: %.2f °C = %.2f °F", cantidad, resultado));
                } else if (rdbFa.isChecked()) {
                    resultado = fahrenheitToCelsius(cantidad);
                    txtResultado.setText(String.format("Resultado: %.2f °F = %.2f °C", cantidad, resultado));
                } else {
                    Toast.makeText(conversion_Grados.this, "Por favor, seleccione un tipo de conversión", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Listener para el botón Limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCantidad.setText("");
                txtResultado.setText("Resultado: ");
                rgConversion.clearCheck();
                rdbCel.setChecked(true);  // Esto asegura que siempre haya una opción seleccionada
            }
        });

        // Listener para el botón Cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); // Cierra la actividad
            }
        });
    }

    private double celsiusToFahrenheit(double celsius) {
        return (celsius * 9 / 5) + 32;
    }

    private double fahrenheitToCelsius(double fahrenheit) {
        return (fahrenheit - 32) * 5 / 9;
    }
}
