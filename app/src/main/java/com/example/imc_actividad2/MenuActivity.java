package com.example.imc_actividad2;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MenuActivity extends AppCompatActivity {

    private CardView crvPrimer,crvImc,crvCambio,crvConversion,crvCotizacion,crvSpineer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_menu);

        iniciarComponentes();
        //codificar los eventos clic de la tarjetas

        crvPrimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this,HolaActivity.class);
                startActivity(intent);
            }
        });
        crvImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this,MainActivity.class);
                //intent.putExtra(nombre);
                startActivity(intent);
            }
        });
        crvConversion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this,conversion_Grados.class);
                //intent.putExtra(nombre);
                startActivity(intent);
            }
        });
        crvCambio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this,monedasActivity.class);
                //intent.putExtra(nombre);
                startActivity(intent);
            }
        });
        crvCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this,cotizacionMenuActivity.class);
                //intent.putExtra(nombre);
                startActivity(intent);
            }
        });
        crvSpineer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this,SpinnerActivity.class);
                //intent.putExtra(nombre);
                startActivity(intent);
            }
        });


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        crvPrimer = (CardView) (findViewById(R.id.crvHola));
        crvImc = (CardView) findViewById(R.id.crvImc);
        crvCambio = (CardView) findViewById(R.id.crvCambio);
        crvConversion = (CardView) findViewById(R.id.crvConversion);
        crvCotizacion = (CardView) findViewById(R.id.crvCotizacion);
        crvSpineer = (CardView) findViewById(R.id.crvSpineer);
    }
}